package com.bancolombia.recaudos.apis.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bancolombia.recaudos.models.Calculator;


@RestController
@CrossOrigin(origins = "*")
public class CalculatorController {

	
	@RequestMapping("/calculator/sum")
	public Calculator sum(@RequestBody Calculator calculator) {
		int result = calculator.getNumber1() + calculator.getNumber2(); 
		Calculator sum = new Calculator();
		sum.setNumber1(calculator.getNumber1());
		sum.setNumber2(calculator.getNumber2());
		sum.setResult(result);
		return sum;
	}

}
